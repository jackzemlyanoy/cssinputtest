var gulp = require('gulp')
var sass = require('gulp-sass')
var browserSync = require('browser-sync')


gulp.task('sass', function () {
    gulp.src('./project/**/*.scss')
        .pipe(sass({includePaths: ['scss']}))
        .pipe(gulp.dest('./project'))
})

gulp.task('browser-sync', function() {
    browserSync.init(["css/*.css"], {
        server:  "./project"
    });
    browserSync.watch('project/**/*.*').on('change', browserSync.reload)
});



gulp.task('default', ['sass', 'browser-sync'], function () {
    // gulp.watch("./project/**/*.scss", ['sass']);
    gulp.paralle(gulp.watch("./project/**/*.scss", ['sass']), 'browser-sync')
});
